data "tfe_team" "owners" {
  depends_on   = [tfe_organization.org]
  name         = "owners"
  organization = local.org
}

resource "tfe_organization_membership" "owners" {
  count        = length(var.re_email_list)
  organization = local.org
  email        = var.re_email_list[count.index]
  depends_on = [tfe_organization.org]
}

resource "tfe_team_organization_member" "owners_member" {
  count                       = length(var.re_email_list)
  team_id                     = data.tfe_team.owners.id
  organization_membership_id  = tfe_organization_membership.owners[count.index].id
  depends_on = [tfe_organization.org]
}
