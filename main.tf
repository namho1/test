provider "tfe" {
  hostname = "35.182.220.227"
  token = "rDBTly4pqpBZWQ.atlasv1.LoHPLjy3wFyMJD2pKy7rtxo4pbhjcD1PyJBeLE0Qslo5zUpNAIhqXAyu6ZqXVidKJ2Y"
}

locals {
  org = "cabbage"
}
resource "tfe_organization" "org" {
  name = local.org
  email = "test@test.test"
}

resource "tfe_team" "engineer" {
  depends_on = [tfe_organization.org]
  name = "engineer-role"
  organization = local.org
}

resource "tfe_team" "re" {
  depends_on = [tfe_organization.org]
  name = "re-role"
  organization = local.org
  organization_access {
    manage_policies = false
    manage_workspaces = true
    manage_vcs_settings = true
  }
}

resource "tfe_team" "readonly" {
  depends_on = [tfe_organization.org]
  name = "readonly-role"
  organization = local.org

  organization_access {
    manage_policies = false
    manage_workspaces = false
    manage_vcs_settings = false
  }
}

resource "tfe_team" "admin" {
  depends_on = [tfe_organization.org]
  name = "admin-role"
  organization = local.org

  organization_access {
    manage_policies = true
    manage_workspaces = true
    manage_vcs_settings = true
  }
}