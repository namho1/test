variable "re_email_list" {
  type        = list(string)
  default     = ["christopher_g_ohara@asnewyorklife.com",
                 "srujana_m_malisetty@asnewyorklife.com",
                 "robert_soltys@asnewyorklife.com"]
}
